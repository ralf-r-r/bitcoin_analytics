from __future__ import division, absolute_import, print_function

from airflow.plugins_manager import AirflowPlugin

import operators

# Defining the plugin class
class BitcoinPlugin(AirflowPlugin):
    name = "bitcoin_plugin"
    operators = [
        operators.CoinGeckoToS3
    ]

from airflow.hooks.S3_hook import S3Hook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
import requests
import numpy as np
from io import BytesIO
import pandas as pd
import time
from datetime import date, timedelta

class CoinGeckoToS3(BaseOperator):
    ui_color = '#358140'
    template_fields = ('execution_date',)

    @apply_defaults
    def __init__(self,
                 s3_conn_id: str,
                 s3_bucket_name: str,
                 endpoint: str,
                 execution_date: str,
                 *args,
                 **kwargs):
        """
        :param s3_conn_id: str, aws credentials id
        :param s3_bucket_name: str, redshift connection id
        :param endpoint: str, the name of the target table
        :param execution_date: str, the region of the s3 bucket
        """
        super(CoinGeckoToS3, self).__init__(*args, **kwargs)

        self.s3_conn_id = s3_conn_id
        self.s3_bucket_name = s3_bucket_name
        self.endpoint = endpoint
        self.execution_date = execution_date

    def execute(self, context):

        # compute start and end date as UNIX UTC time stamp
        end_date_str = self.execution_date.format(**context)
        date_end = date.fromisoformat(end_date_str)
        date_start = date_end - timedelta(days=1)

        ts_end = int(time.mktime(date_end.timetuple()))
        ts_start = int(time.mktime(date_start.timetuple()))

        params = {"vs_currency": "usd",
                  "days": 1,
                  "from": ts_start,
                  "to": ts_end
                  }

        # get data from endpoint and extract data in json format
        self.log.info('start api call with endpoint:' + self.endpoint)
        result = requests.get(url=self.endpoint, params=params)
        data = result.json()
        self.log.info('Api call with endpoint:' + self.endpoint + 'successful !')

        # convert json to csv and copy data to S3 bucket
        # time interval of the data received from coin gecko API is hourly
        self.log.info('start copying data to s3 bucket' + self.s3_bucket_name)

        prices = np.asarray(data['prices'])
        volumes = np.asarray(data['total_volumes'])

        result_dict = {
            "timestamp": prices[:, 0],
            "usd": prices[:, 1],
            "volume_usd": volumes[:, 1],
        }

        df = pd.DataFrame(result_dict)

        csv_buffer = BytesIO()
        df.to_csv(csv_buffer, header=True, index=False)
        csv_buffer.seek(0)

        s3 = S3Hook(self.s3_conn_id)
        key = str(ts_end) + "_bitcoin_data.csv"
        s3.load_bytes(bytes_data=csv_buffer, bucket_name='rrrbitcoinbucket', key=key)

        self.log.info('copying data to s3 bucket' + self.s3_bucket_name + " successful!")

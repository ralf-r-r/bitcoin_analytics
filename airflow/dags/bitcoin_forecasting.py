from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from operators import CoinGeckoToS3

# - define start date, end date and schedule interval
default_args = {
    'owner': 'udacity',
    'start_date': datetime(2020, 12, 30),
    'end_date': datetime(2021, 2, 10),
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
    'email_on_retry': False,
    'catchup': True,
    'depends_on_past': False
}

dag = DAG('bitcoin_forecasting',
          default_args=default_args,
          description='Load and transform data in Redshift with Airflow, run time series rpediction',
          schedule_interval='0 0 * * *',
          )

# - define start_operator
start_operator = DummyOperator(task_id='Begin_execution', dag=dag)

# - define staging operators
coin_gecko_to_s3 = CoinGeckoToS3(
    task_id='Stage_events',
    dag=dag,
    s3_conn_id='tbd',
    s3_bucket_name='rrrbitcoinbucket',
    endpoint="https://api.coingecko.com/api/v3/coins/bitcoin/market_chart/range",
    execution_date='{ds}'
)

end_operator = DummyOperator(task_id='Stop_execution', dag=dag)

# - define task dependencies
start_operator >> coin_gecko_to_s3
coin_gecko_to_s3 >> end_operator
